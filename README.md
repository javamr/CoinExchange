## 简要介绍

本项目是基于Java（SpringCloud）开发的比特币交易所 | BTC交易所 | ETH交易所 | 数字货币交易所 | 交易平台 | 撮合交易引擎。本项目基于SpringCloud微服务开发，可用来搭建和二次开发数字货币交易所，有完整的系统组成部分。 

- 撮合交易引擎 
- 后台管理（后端+前端） 
- 前台（交易页面、活动页面、个人中心等） 
- 原生安卓APP源码 
- 原生苹果APP源码 
- 币种钱包RPC源码

## 实际演示网站

[https://www.bizzan.com](https://www.bizzan.com)
 
## 系统架构概要

#### 整体架构

![整体架构](https://images.gitee.com/uploads/images/2020/0407/143836_eac248e5_2182501.png "1.png")

#### 逻辑架构

![逻辑架构](https://images.gitee.com/uploads/images/2020/0407/143856_66257325_2182501.png "2.png")

#### 部署架构

![部署架构](https://images.gitee.com/uploads/images/2020/0408/141710_07923003_2182501.png "1117.png")

## 关于服务器配置与部署

如果你想在自己的电脑或云服务器上搭建一套交易所系统，我这里准备了一些基本的部署手册，当然，在linux/unix上安装软件并不是一件简单的事，你需要有一定的linux基础和命令行功底，同时还要有遇到问题解决问题的勇气和耐心，祝你顺利！

- [服务器配置参考手册](./09_DOC/00_服务器部署/推荐服务器配置.md)  
- [安装基础环境手册](./09_DOC/00_服务器部署/安装基础环境.md)  
- [服务部署脚本](./09_DOC/00_服务器部署/install.sh)  
- [安装MySql手册](./09_DOC/00_服务器部署/安装MySql.md)  
- [安装Redis手册](./09_DOC/00_服务器部署/安装Redis.md)  
- [安装Zookeeper手册](./09_DOC/00_服务器部署/安装Zookeeper.md)  
- [安装Kafka手册](./09_DOC/00_服务器部署/安装Kafka.md)  
- [安装Mongodb手册](./09_DOC/00_服务器部署/安装Mongodb.md)  
- [搭建BTC钱包节点手册](./09_DOC/00_服务器部署/搭建BTC钱包.md)  
- [搭建ETH钱包节点手册](./09_DOC/00_服务器部署/搭建ETH钱包.md)  
- [搭建USDT钱包节点手册](./09_DOC/00_服务器部署/搭建USDT钱包.md)  

## 关于撮合交易引擎

本系统对交易队列采用内存撮合的方式进行，以Kafka做撮合订单信息传输，MongoDB持久化订单成交明细，MySQL记录订单总体成交。
其中01_Framework/Exchange项目主要负责内存撮合，01_Framework/Market项目主要负责订单成交持久化、行情生成、行情推送等服务，包括：

- K线数据，间隔分别为：1分钟、5分钟、15分钟、30分钟、1小时、1天、1周、1月
- 所有交易对的市场深度（market depth）数据
- 所有交易对的最新价格
- 最近成交的交易对

 **内存撮合交易支持的模式** 

- 限价订单与限价订单撮合
- 市价订单与限价订单撮合
- 限价订单与市价订单撮合
- 市价订单与市价订单撮合

 **限价&市价订单处理逻辑** 

![限价&市价订单处理逻辑](https://images.gitee.com/uploads/images/2020/0408/144633_457c0552_2182501.png "2222.png")

 **撮合引擎支持的其他功能** 

除了普通的限价与市价撮合交易功能外，本系统的撮合交易引擎还引入了活动成交模式，通过设置交易对（如：BTC/USDT）的开始交易时间、初始发行量、初始发行价、活动模式等参数，可以制定出丰富的撮合交易模式，从而满足不同的撮合模式。

 **总结** 

总之，本系统支持高度自定义的撮合模式，同时你也可以开发出你自己想要的撮合交易模式，只需要通过修改Exchange项目中的撮合逻辑就可以。

## 关于技术构成

- 后端：Spring、SpringMVC、SpringData、SpringCloud、SpringBoot
- 数据库：Mysql、Mongodb
- 其他：redis、kafka、阿里云OSS、腾讯防水校验
- 前端：Vue、iView、less

> 关于交易机器人   
> 交易机器人是自动交易的程序，可以根据外部行情，自动进行交易，让本交易所的交易对价格与外部保持一致，防止因部分用户“搬砖”导致的损失。   

> 关于搬砖  
> 例如A交易所BTC价格是10000USDT，而B交易所的BTC价格是9500USDT，搬砖就是在B交易所通过9500USDT的价格买入BTC，然后转账到A交易所，赚取差价（500USDT）。    
> 如果交易所没有交易机器人，就会导致本交易所的币种价格与其他主流交易所相比有差价，从而让用户“搬砖”，导致交易所损失。   
> 另外，交易机器人还有一个功能，就是在交易所初期运营的时候，形成一个初期的交易深度，不会让用户觉得交易所冷清，没有用户。   

## 文件目录说明

 **00_framework**

└─admin   后台管理API

└─bitrade-job     任务管理

└─chat   OTC聊天

└─cloud  SpringCloud微服务管理

└─core   核心

└─exchange   撮合交易引擎

└─exchange-api   撮合交易API

└─exchange-core   撮合交易核心

└─jar   第三方类库

└─market    市场行情API、K线生成

└─otc-api   OTC交易API（如无需otc功能可不启动）

└─otc-core  OTC核心

└─sql    SQL脚本

└─ucenter-api    用户个人中心API

└─wallet      钱包资产管理，负责与RPC对接


 **01_wallet_rpc**

└─bitcoin

└─bsv

└─btm

└─eos

└─erc-eusdt

└─erc-token（可对接各种ERC20币种）

└─eth

└─ltc

└─usdt


 **02_App_Android**

 **03_App_IOS**

 **04_Web_Admin**

 **05_Web_Front**

## 核心功能说明（用户端）

- [x] 1.  注册/登录/实名认证/审核（目前仅支持手机，二次开发可加入邮件，很简单）
- [x] 2.  Banner/公告/帮助/定制页面（Banner支持PC与APP分开设置，帮助支持各种分类模式）
- [x] 3.  法币C2C交易/法币OTC交易（支持两种法币模式，项目早期可由平台承担C2C法币兑换，后期可开放OTC交易）
- [x] 4.  币币交易（支持限价委托、市价委托，二次开发可加入其它委托模式）
- [x] 5.  邀请注册/推广合伙人（支持对邀请推广人数、佣金进行以日、周、月的排行统计）
- [x] 6.  创新实验室（该部分支持功能较多，分项说明。另，APP暂不全部支持该功能）

>    - [x] 6-1.  首发抢购活动模式（如发行新交易对时，可对交易对设置一定数量的币种进行抢购）  
>    - [x] 6-2.  首发分摊活动模式（如发行BTC/USDT交易对之前，官方拿出5BTC做活动，根据用户充值抵押的USDT多少进行均分BTC）  
>    - [x] 6-3.  控盘抢购模式（如发行ZZZ/USDT交易对之前，ZZZ币种价格为5USDT，官方发行活动价为0.5USDT，则可使用该模式）  
>    - [x] 6-4.  控盘均摊模式（如6-3，只不过平均分配）  
>    - [x] 6-5.  矿机活动模式（支持用户抵押一定数量的币种，由官方承诺每月返还一定数量的币种）  

- [x] 7.  红包功能（支持平台及官方发放一定数量币种的红包，此功能适合用户裂变）
- [x] 8.  用户资产管理、流水管理、委托管理、实名管理等各种基础管理


## 核心功能说明（管理端）

- [x] 1.  概要（查看平台运行数据，包含交易额、注册人数、充值等）
- [x] 2.  会员管理（会员信息管理、会员实名审核、会员实名管理、会员余额管理、会员充值/冻结余额等）
- [x] 3.  邀请管理（会员邀请信息、会员邀请排行管理）
- [x] 4.  CTC管理（CTC订单管理、流水管理、承兑商管理）
- [x] 5.  内容管理（PC广告管理、APP广告管理、公告管理、帮助管理）
- [x] 6.  财务管理（充值提现管理、财务流水管理、对账管理、币种钱包余额管理）
- [x] 7.  币币管理（新建交易对、管理交易对、新建交易机器人、设置交易机器人参数、设置行情引擎/交易引擎、撤销所有委托）
- [x] 8.  活动管理（新建活动、矿机认购、抢购/瓜分管理）
- [x] 9.  红包管理（平台红包管理、用户红包管理）
- [x] 10.  系统管理（角色管理、部门管理、用户管理、权限管理、币种管理、RPC管理、版本管理）
- [x] 11.  保证金管理（此功能设计时考虑到，但实际运营期间未使用到）
- [x] 12.  OTC管理（广告管理、订单管理、OTC币种管理、退保管理等，此功能未获得实际运营检验）


## 系统展示（PC前端）

![首页](https://images.gitee.com/uploads/images/2020/0327/135803_75ec9a0b_2182501.png "01_首页.png")

![币币交易](https://images.gitee.com/uploads/images/2020/0327/135834_4a5fb1c4_2182501.png "02_币币交易.png")

![法币交易](https://images.gitee.com/uploads/images/2020/0327/135902_a7286b9c_2182501.png "03_法币交易CTC.png")

![登录](https://images.gitee.com/uploads/images/2020/0322/193759_edc5dc7b_2182501.png "图片5.png")

![活动/创新实验室](https://images.gitee.com/uploads/images/2020/0327/135930_0c02d004_2182501.png "04_创新实验室.png")

![创新实验室详情](https://images.gitee.com/uploads/images/2020/0327/140037_074a81a4_2182501.png "创新实验室详情.png")

![推广合伙人](https://images.gitee.com/uploads/images/2020/0327/140003_9b962fe7_2182501.png "07_推广合伙人.png")

![公告](https://images.gitee.com/uploads/images/2020/0322/193852_3ad12a6f_2182501.png "图片8.png")

![帮助](https://images.gitee.com/uploads/images/2020/0322/193902_ef09925e_2182501.png "图片9.png")

## 系统运行展示（APP前端）

![首页](https://images.gitee.com/uploads/images/2020/0322/193927_9940ca7c_2182501.jpeg "图片10.jpg")

![行情](https://images.gitee.com/uploads/images/2020/0322/193941_ff5a16a2_2182501.jpeg "图片11.jpg")

![K线](https://images.gitee.com/uploads/images/2020/0322/193951_abf7b5b6_2182501.jpeg "图片12.jpg")

![交易](https://images.gitee.com/uploads/images/2020/0322/194003_d14a772a_2182501.jpeg "图片13.jpg")

![个人中心](https://images.gitee.com/uploads/images/2020/0322/194021_a047d3a5_2182501.jpeg "图片14.jpg")

![个人资产管理](https://images.gitee.com/uploads/images/2020/0322/194059_faeeeb4a_2182501.jpeg "图片15.jpg")

![邀请管理](https://images.gitee.com/uploads/images/2020/0322/194112_7ae11b00_2182501.jpeg "图片16.jpg")


## 系统运行展示（后端）

![登录](https://images.gitee.com/uploads/images/2020/0322/194251_9b5293ff_2182501.png "图片17.png")

![首页](https://images.gitee.com/uploads/images/2020/0322/194305_f83e4f90_2182501.png "图片18.png")

![用户管理](https://images.gitee.com/uploads/images/2020/0322/194321_73eb8f58_2182501.png "图片19.png")

![邀请管理](https://images.gitee.com/uploads/images/2020/0322/194337_fd257186_2182501.png "图片20.png")

![法币交易订单管理](https://images.gitee.com/uploads/images/2020/0322/194406_ebe7328d_2182501.png "图片21.png")

![首页Banner管理](https://images.gitee.com/uploads/images/2020/0322/194433_4fb39b0a_2182501.png "图片22.png")

![交易对管理](https://images.gitee.com/uploads/images/2020/0322/194450_1eb7bb6f_2182501.png "图片23.png")

![活动管理](https://images.gitee.com/uploads/images/2020/0322/194505_204d23ce_2182501.png "图片24.png")

![红包管理](https://images.gitee.com/uploads/images/2020/0322/194531_e12eb93a_2182501.png "图片25.png")

![币种管理](https://images.gitee.com/uploads/images/2020/0322/194618_fe17409a_2182501.png "图片26.png")

![OTC管理，后端开发完成，前端未对接](https://images.gitee.com/uploads/images/2020/0322/194654_bd0acbe7_2182501.png "图片27.png")
